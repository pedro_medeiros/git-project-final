package controller;


import model.Games;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.GamesService;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/games")
public class GamesController {

    private final List<Games> game;

    @Autowired
    private GamesService gamesService;

    public GamesController(){
        this.game = new ArrayList<Games>();
        Games game1 = new Games(01, "The Witcher 3", "The Witcher 3: Wild Hunt é o terceiro capítulo da saga de Geralt of Rivia, baseado no livro do famoso escritor polonês Andrzej Sapkowski, disponível para PS4, Xbox One, Nintendo Switch e PC.", 79.99, 0.25, 34);
        Games game2 = new Games(02, "Cupehead", "Cuphead é um jogo de tiro (run 'n' gun) e plataforma disponível para Xbox One e PC e com uma grande ênfase na luta contra chefões.", 36.99, 0.30, 12);
        Games game3 = new Games(03, "Stardew valley", "Stardew Valley é um jogo de simulação de fazenda primariamente inspirado pela série de videogames Harvest Moon.", 24.99, 0.10, 40);
        Games game4 = new Games(04, "Metal Gear Solid V: The Phantom Pain", "Metal Gear Solid V: The Phantom Pain é um jogo eletrônico de ação-aventura furtiva desenvolvido pela Kojima Productions, co-produzido pela Kojima Productions Los Angeles e realizado, desenhado, co-produzido e co-escrito por Hideo Kojima.", 129.00, 0.50, 14);
        Games game5 = new Games(05, "Little Nightmares", "Little Nightmares é um jogo eletrônico de quebra-cabeça em plataforma com elementos de terror desenvolvido pela Tarsier Studios e publicado pela Bandai Namco Entertainment.", 79.99, 0.12, 20);
        Games game6 = new Games(06, "Left 4 Dead", "Left 4 Dead é um jogo multiplayer cooperativo de survival horror e tiro em primeira pessoa inicialmente desenvolvido pela Turtle Rock Studios que passou, no dia 10 de Janeiro de 2008, a fazer parte da corporação Valve.", 20.69, 0.80, 6);
        Games game7 = new Games(07, "A Way Out", "A Way Out é um jogo eletrônico de ação-aventura desenvolvido pela Hazelight Studios e publicado pela Electronic Arts sob a marca de EA Originals.", 89.00, 0.90, 17);
        game.add(game1);
        game.add(game2);
        game.add(game3);
        game.add(game4);
        game.add(game5);
        game.add(game6);
        game.add(game7);
    }

    @GetMapping
    public List<Games> findAll(@RequestParam(required = false) Games games) {
        return gamesService.findAll(games);
    }

    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody final Games games){
        Integer id = gamesService.add(games);
        return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody final Games games){
        gamesService.update(games);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") final Integer id){
        gamesService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
