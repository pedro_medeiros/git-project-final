package menu;

import model.Games;

import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {
        Games game[] = new Games[7];
        Games g1 = new Games(01, "The Witcher 3", "The Witcher 3: Wild Hunt é o terceiro capítulo da saga de Geralt of Rivia, baseado no livro do famoso escritor polonês Andrzej Sapkowski, disponível para PS4, Xbox One, Nintendo Switch e PC.", 79.99, 0.25, 34);
        Games g2 = new Games(02, "Cupehead", "Cuphead é um jogo de tiro (run 'n' gun) e plataforma disponível para Xbox One e PC e com uma grande ênfase na luta contra chefões.", 36.99, 0.30, 12);
        Games g3 = new Games(03, "Stardew valley", "Stardew Valley é um jogo de simulação de fazenda primariamente inspirado pela série de videogames Harvest Moon.", 24.99, 0.10, 40);
        Games g4 = new Games(04, "Metal Gear Solid V: The Phantom Pain", "Metal Gear Solid V: The Phantom Pain é um jogo eletrônico de ação-aventura furtiva desenvolvido pela Kojima Productions, co-produzido pela Kojima Productions Los Angeles e realizado, desenhado, co-produzido e co-escrito por Hideo Kojima.", 129.00, 0.50, 14);
        Games g5 = new Games(05, "Little Nightmares", "Little Nightmares é um jogo eletrônico de quebra-cabeça em plataforma com elementos de terror desenvolvido pela Tarsier Studios e publicado pela Bandai Namco Entertainment.", 79.99, 0.12, 20);
        Games g6 = new Games(06, "Left 4 Dead", "Left 4 Dead é um jogo multiplayer cooperativo de survival horror e tiro em primeira pessoa inicialmente desenvolvido pela Turtle Rock Studios que passou, no dia 10 de Janeiro de 2008, a fazer parte da corporação Valve.", 20.69, 0.80, 6);
        Games g7 = new Games(07, "A Way Out", "A Way Out é um jogo eletrônico de ação-aventura desenvolvido pela Hazelight Studios e publicado pela Electronic Arts sob a marca de EA Originals.", 89.00, 0.90, 17);
        game[0] = g1;
        game[1] = g2;
        game[2] = g3;
        game[3] = g4;
        game[4] = g5;
        game[5] = g6;
        game[6] = g7;
        while (true) {
            System.out.println("[Menu]");
            System.out.println("1-[Abrir lista de jogos disponíveis para compra]");
            System.out.println("2-[Sair da página]");
            Scanner menu = new Scanner(System.in);
            int opcao = menu.nextInt();
            switch (opcao) {
                case 1:
                    System.out.print("1- " + g1.getNome());
                    System.out.println(" R$:" + g1.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g1.getEstoque());
                    System.out.println("Descrição do produto:" + g1.getDescricao());
                    System.out.print("2- " + g2.getNome());
                    System.out.println(" R$:" + g2.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g2.getEstoque());
                    System.out.println("Descrição do produto:" + g2.getDescricao());
                    System.out.print("3- " + g3.getNome());
                    System.out.println(" R$:" + g3.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g3.getEstoque());
                    System.out.println("Descrição do produto:" + g3.getDescricao());
                    System.out.print("4- " + g4.getNome());
                    System.out.println(" R$:" + g4.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g4.getEstoque());
                    System.out.println("Descrição do produto:" + g4.getDescricao());
                    System.out.print("5- " + g5.getNome());
                    System.out.println(" R$:" + g5.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g5.getEstoque());
                    System.out.println("Descrição do produto:" + g5.getDescricao());
                    System.out.print("6- " + g6.getNome());
                    System.out.println(" R$:" + g6.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g6.getEstoque());
                    System.out.println("Descrição do produto:" + g6.getDescricao());
                    System.out.print("7- " + g7.getNome());
                    System.out.println(" R$:" + g7.getValor());
                    System.out.println("Quantidade de cópias disponiveis para compra: " + g7.getEstoque());
                    System.out.println("Descrição do produto:" + g7.getDescricao());
                    break;
                case 2:
                    System.out.println("Operação encerrada.");
                    System.exit(0);
                    break;
            }
            System.out.println("Escolha a numeração do jogo que você deseja comprar");
            System.out.println("Digite 8 para fechar a página.");
            int compra = menu.nextInt();
            switch (compra){
                case 1:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada = new Scanner(System.in);
                    double quantidadeLevada = Double.parseDouble(quantidadeComprada.next());
                    g1.compra(g1.getEstoque(), quantidadeLevada, g1.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto = new Scanner(System.in);
                    double descontoUtilizado = Double.parseDouble(valorDesconto.next());
                    g1.aplicar_desconto(g1.getValor(), descontoUtilizado);
                    System.out.println("Compra efetuada com sucesso pelo valor R$:" + g1.getValor());
                    break;
                case 2:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada2 = new Scanner(System.in);
                    double quantidadeLevada2 = Double.parseDouble(quantidadeComprada2.next());
                    g2.compra(g2.getEstoque(), quantidadeLevada2, g2.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto2 = new Scanner(System.in);
                    double descontoUtilizado2 = Double.parseDouble(valorDesconto2.next());
                    g2.aplicar_desconto(g2.getValor(), descontoUtilizado2);
                    System.out.println(g2.getNome() + " comprado com sucesso pelo valor R$:" + g2.getValor());
                    break;
                case 3:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada3 = new Scanner(System.in);
                    double quantidadeLevada3 = Double.parseDouble(quantidadeComprada3.next());
                    g3.compra(g3.getEstoque(), quantidadeLevada3, g3.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto3 = new Scanner(System.in);
                    double descontoUtilizado3 = Double.parseDouble(valorDesconto3.next());
                    g3.aplicar_desconto(g3.getValor(), descontoUtilizado3);
                    System.out.println(g3.getNome() + " comprado com sucesso pelo valor R$:" + g3.getValor());
                    break;
                case 4:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada4 = new Scanner(System.in);
                    double quantidadeLevada4 = Double.parseDouble(quantidadeComprada4.next());
                    g4.compra(g4.getEstoque(), quantidadeLevada4, g4.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto4 = new Scanner(System.in);
                    double descontoUtilizado4 = Double.parseDouble(valorDesconto4.next());
                    g4.aplicar_desconto(g4.getValor(), descontoUtilizado4);
                    System.out.println(g4.getNome() + " comprado com sucesso pelo valor R$:" + g4.getValor());
                    break;
                case 5:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada5 = new Scanner(System.in);
                    double quantidadeLevada5 = Double.parseDouble(quantidadeComprada5.next());
                    g5.compra(g5.getEstoque(), quantidadeLevada5, g5.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto5 = new Scanner(System.in);
                    double descontoUtilizado5 = Double.parseDouble(valorDesconto5.next());
                    g5.aplicar_desconto(g5.getValor(), descontoUtilizado5);
                    System.out.println(g5.getNome() + " comprado com sucesso pelo valor R$:" + g5.getValor());
                    break;
                case 6:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada6 = new Scanner(System.in);
                    double quantidadeLevada6 = Double.parseDouble(quantidadeComprada6.next());
                    g6.compra(g6.getEstoque(), quantidadeLevada6, g6.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto6 = new Scanner(System.in);
                    double descontoUtilizado6 = Double.parseDouble(valorDesconto6.next());
                    g6.aplicar_desconto(g6.getValor(), descontoUtilizado6);
                    System.out.println(g6.getNome() + " comprado com sucesso pelo valor R$:" + g6.getValor());
                    break;
                case 7:
                    System.out.println("Digite a quantidade que deseja comprar: ");
                    Scanner quantidadeComprada7 = new Scanner(System.in);
                    double quantidadeLevada7 = Double.parseDouble(quantidadeComprada7.next());
                    g7.compra(g7.getEstoque(), quantidadeLevada7, g7.getValor());
                    System.out.print("Gostaria de usar um cupom de desconto na loja? se sim, digite o valor do desconto, caso contrário digite 1 para efetuar a compra no seu preço original.");
                    Scanner valorDesconto7 = new Scanner(System.in);
                    double descontoUtilizado7 = Double.parseDouble(valorDesconto7.next());
                    g7.aplicar_desconto(g7.getValor(), descontoUtilizado7);
                    System.out.println(g7.getNome() + " comprado com sucesso pelo valor R$:" + g7.getValor());
                    break;
                case 8:
                    System.out.println("Operação encerrada.");
                    System.exit(0);
                    break;
            }
        }
    }
}
