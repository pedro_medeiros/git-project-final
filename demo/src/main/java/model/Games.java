package model;

public class Games {
    private Integer id;
    private String nome;
    private String descricao;
    private double valor;
    private double valor_maximo_de_desconto;
    private double estoque;

    public Games(Integer id, String nome, String descricao, double valor, double valor_maximo_de_desconto, double estoque) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.valor_maximo_de_desconto = valor_maximo_de_desconto;
        this.estoque = estoque;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValor_maximo_de_desconto() {
        return valor_maximo_de_desconto;
    }

    public void setValor_maximo_de_desconto(double valor_maximo_de_desconto) {
        this.valor_maximo_de_desconto = valor_maximo_de_desconto;
    }

    public double getEstoque() {
        return estoque;
    }

    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }
    public void aplicar_desconto(double valor, double desconto){
        if(desconto > valor_maximo_de_desconto){
            System.out.println("Desconto inválido, por favor reinicie a operação e utilize um cupom que não ultrapasse o valor máximo exigido.");
        }
        this.valor = this.valor*desconto;
    }
    public void compra(double estoque, double quantidade_comprada, double valor){
        if(estoque < 1){
            System.out.println("Sentimos muito, mas o produto desejado já está esgotado.");
        }
        this.valor = this.valor*quantidade_comprada;
        this.estoque = this.estoque - quantidade_comprada;
    }
}
