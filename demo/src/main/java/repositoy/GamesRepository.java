package repositoy;

import model.Games;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class GamesRepository {

    private final List<Games> game;

    public GamesRepository() {
        this.game = new ArrayList<>();
    }

    public List<Games> findAll() {
        return game;
    }

    public List<Games> findAll(final Games games){
            return game.stream()
                    .filter(gme -> gme.getNome().contains(games.getNome()))
                    .collect(Collectors.toList());
        }

    public Integer add(Games games){
        this.game.add(games);
        return add(games);
    }

    public List<Games> update(Games games){
        game.stream()
                .filter(gme -> gme.getId().equals(games.getId()))
                .forEach(gme -> gme.setNome(games.getNome()));
        game.stream()
                .filter(gme -> gme.getId().equals(games.getId()))
                .forEach(gme -> gme.setDescricao(games.getDescricao()));
        game.stream()
                .filter(gme -> gme.getId().equals(games.getId()))
                .forEach(gme -> gme.setValor(games.getValor()));
        game.stream()
                .filter(gme -> gme.getId().equals(games.getId()))
                .forEach(gme -> gme.setValor_maximo_de_desconto(games.getValor_maximo_de_desconto()));
        game.stream()
                .filter(gme -> gme.getId().equals(games.getId()))
                .forEach(gme -> gme.setEstoque(games.getEstoque()));
        return update(games);
    }
    public List<Games> delete(Integer id){
        game.removeIf(gme -> gme.getId().equals(id));
        return delete(id);
    }

    public int count(){
        return game.size();
    }
}
