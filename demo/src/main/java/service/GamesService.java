package service;

import model.Games;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositoy.GamesRepository;
import java.util.List;

@Service
public class GamesService {

    @Autowired
    private GamesRepository gamesRepository;

    public List<Games> findAll (final Games games) {
        if (games.getNome() != null) {
            return gamesRepository.findAll(games);
        }
        return gamesRepository.findAll();
    }

    public Integer add(final Games games){
        if (games.getId() == null) {
            games.setId(gamesRepository.count() + 1);
        }
        gamesRepository.add(games);
        return games.getId();
    }
    public List<Games> update(final Games games){
        return gamesRepository.update(games);
    }

    public List<Games> delete(Integer id){
        return gamesRepository.delete(id);
    }
}
